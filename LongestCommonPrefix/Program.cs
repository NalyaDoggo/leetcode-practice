﻿using System;

namespace LongestCommonPrefix
{
    class Program
    { 
        public static string LongestCommonPrefix(string[] strs)
        { 
            string c = string.Empty;
            bool check = false;
            if (strs.Length == 1)
            {
                return strs[0];
            }
            
            else
            {
                for (int i = 1; i <= strs[0].Length; i++)
                {
                    for (int j = 0; j < strs.Length; j++)
                    {
                        if (strs[j].StartsWith(strs[0].Substring(0, i)) == true)
                        {
                            check = true;
                        }
                        else
                        {
                            check = false;
                            break;
                        }

                    }
                    if (check == true)
                    {
                        c = strs[0].Substring(0, i);
                    }
                }
                return c;
            }
        }

        /* Leetcode solution: Horizontal slicing
        public static string LongestCommonPrefix(String[] strs) {
            if (strs.Length == 0) return "";
            String prefix = strs[0];
            for (int i = 1; i < strs.Length; i++)
                while (strs[i].IndexOf(prefix) != 0) {
                    prefix = prefix.Substring(0, prefix.Length - 1);
                    if (prefix.Length == 0) return "";
                }        
            return prefix;
        }
        */
        static void Main(string[] args)
        {
            string[] cat = new string[] { "","b"};
            Console.WriteLine("Common: " + LongestCommonPrefix(cat));
        }
    }
}
