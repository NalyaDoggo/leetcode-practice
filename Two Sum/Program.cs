﻿using System;

namespace Two_Sum
{
    class Program
    {
        public static int[] TwoSum(int[] nums, int target)
        {
            for (int i = 0; nums.Length > i; i++)
            {
                for (int j = 0; nums.Length > j; j++)
                {
                    if (i == j)
                    {
                        continue;
                    }
                    else if (nums[i] + nums[j] == target)
                    {
                        int[] arr = new int[] { i, j };
                        return arr;
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            int[] end = new int[] { 0 };
            return end;
        }
        static void Main(string[] args)
        {
            int[] arr = new int[] { 2, 7, 11, 15 };
            foreach(var item in TwoSum(arr, 9))
            {
                Console.WriteLine(item);
            }
        }
    }
}
