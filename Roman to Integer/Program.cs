﻿using System;

namespace Roman_to_Integer
{
    class Program
    {
        enum RomanNums
        {
            I = 1,
            V = 5,
            X = 10,
            L = 50,
            C = 100,
            D = 500,
            M = 1000
        }
        public static int RomanToInt(string s)
        {
            int total = 0;
            try
            { 
                if (s.Length > 1)
                {
                    for (int i = 0; i < s.Length; i++)
                    {
                        try
                        {
                            if (s[i].ToString().Equals("I")) {
                                if (s[i + 1].ToString().Equals("V") || s[i+1].ToString().Equals("X"))
                                {
                                    if (s[i + 1].ToString().Equals("V"))
                                    {
                                        total += 4;
                                        i++;
                                    } else
                                    {
                                        total += 9;
                                        i++;
                                    }
                                } else
                                {
                                    total += 1;
                                }
                            } else if (s[i].ToString().Equals("X")) 
                            {
                                if (s[i + 1].ToString().Equals("L") || s[i + 1].ToString().Equals("C"))
                                {
                                    if (s[i + 1].ToString().Equals("L"))
                                    {
                                        total += 40;
                                        i++;
                                    }
                                    else
                                    {
                                        total += 90;
                                        i++;
                                    }
                                }
                                else
                                {
                                    total += 10;
                                }
                            } else if (s[i].ToString().Equals("C"))
                            {
                                if (s[i + 1].ToString().Equals("D") || s[i + 1].ToString().Equals("M"))
                                {
                                    if (s[i + 1].ToString().Equals("D"))
                                    {
                                        total += 400;
                                        i++;
                                    }
                                    else
                                    {
                                        total += 900;
                                        i++;
                                    }
                                } else
                                {
                                    total += 100;
                                }
                            } else
                            {
                                RomanNums n1 = (RomanNums)Enum.Parse(typeof(RomanNums), s[i].ToString());
                                total += (int)n1;
                            }
                        }
                        catch (IndexOutOfRangeException)
                        {
                            RomanNums n1 = (RomanNums)Enum.Parse(typeof(RomanNums), s[i].ToString());
                            total += (int) n1;
                            return total;
                        }

                    }
                    return total;
                } else 
                {
                    RomanNums n1 = (RomanNums)Enum.Parse(typeof(RomanNums), s);
                    return (int) n1;
                }
            } catch (FormatException)
            {
                Console.WriteLine("Unable to convert");
                return -1;
            }



        }
        static void Main(string[] args)
        {
            int cat = RomanToInt("IV");
            Console.WriteLine("Returned: " + cat);
        }
    }
}
