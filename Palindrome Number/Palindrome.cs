﻿using System;

namespace Palindrome_Number
{
    class Palindrome
    {
        public static bool IsPalindrome(int x)
        {
            string num = x.ToString();
            string newNum = String.Empty;
            string newStr = String.Empty;
            foreach (char c in num)
            {
                if (c.Equals("-"))
                {
                    return false;
                }
                newStr = c + newStr;
            }
            for(int i = 0; i < num.Length; i++)
            {
                if (num[i].Equals(newStr[i]))
                {
                    continue;
                } else
                {
                    return false;
                }
            }
            return true;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
