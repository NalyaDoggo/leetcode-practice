﻿using System;

namespace ReverseInteger
{
    class ReverseInteger
    {
        public static int Reverse(int x)
        {
            string num = x.ToString();
            string minus = string.Empty;
            string newNum = string.Empty;
            foreach(char c in num)
                {
                    if (c.Equals('-'))
                    {
                        minus = "-";
                        continue;
                    }
                    newNum = c + newNum;
                }
            try
            {
                int num2 = Int32.Parse(minus + newNum);
                return num2;
            }
            catch (Exception e)
            {
                return 0;
            }
        }
        static void Main(string[] args)
        {
        }
    }
}
